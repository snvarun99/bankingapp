package com.company;

import java.util.Calendar;
import java.util.Date;

public class Bank {

    public static void main(String[] args)  {

        Account account1= new Account("Mohan",new Address("Bangalore","Karnataka",560021),2000,
                new Date(2020, Calendar.JANUARY,25),Status.ACTIVE,AccountType.SAVINGS);
        account1.deposit(1000);
        try{
            account1.withdraw(2000);
        }
        catch (InsufficientBalanceException e){
            e.printStackTrace();
        }
        try{
            account1.withdraw(3000);
        }
        catch (InsufficientBalanceException e){
            e.printStackTrace();
        }



    }
}
