package com.company;

import java.util.Date;

public class Account {

    private String ownerName;
    private Address address;
    private long balance;
    private Date created_date;
    private Status status;
    private AccountType accountType;


    public Account(String ownerName, Address address, long balance, Date created_date, Status status, AccountType accountType) {
        this.ownerName = ownerName;
        this.address = address;
        this.balance = balance;
        this.created_date = created_date;
        this.status = status;
        this.accountType = accountType;
    }

    public void deposit(long amount){
        balance+=amount;
        System.out.println("Current Balance is "+balance);
    }

    public void withdraw(long amount)  throws  InsufficientBalanceException{

        if(balance>=amount){
            balance-=amount;
            System.out.println("Current Balance is "+balance);
        }
        else throw new InsufficientBalanceException("Insufficient Balance to withdraw");
    }

}
